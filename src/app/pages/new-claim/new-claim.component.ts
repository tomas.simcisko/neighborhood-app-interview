import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { ClaimsService } from '../../common/services/claims/claims.service';
import { AuthService } from '../../common/services/auth/auth.service';
import { Claim } from '../../common/models/claim/claim';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Status } from '../../common/models/claim/status';
import { Location } from '../../common/models/claim/location';
import { toTitleCase } from 'codelyzer/util/utils';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NoticeType } from '../../common/models/claim/noticeType';

@Component({
    selector: 'app-new-claim',
    templateUrl: './new-claim.component.html',
    styleUrls: ['./new-claim.component.scss']
})
export class NewClaimComponent implements OnInit {

    claimForm: FormGroup;
    public locations: Location[];
    public statuses: Status[];
    public noticeTypes: NoticeType[];


    constructor(
        private router: Router,
        private claimService: ClaimsService,
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private matSnackBar: MatSnackBar,
    ) {
    }

    ngOnInit(): void {
        this.getDataForSelect();

        this.claimForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            claimNumber: [null],
            description: [null, Validators.required],
            locations: [null, Validators.required],
            statuses: [null, Validators.required],
            types: [null, Validators.required]
        });
    }

    public saveClaim() {
        const userName = this.authService.userFullName;
        const title = this.claimForm.controls['title'].value;
        const claimNumber = this.claimForm.controls['claimNumber'].value;
        const location = this.claimForm.controls['locations'].value;
        const status = this.claimForm.controls['statuses'].value;
        const description = this.claimForm.controls['description'].value;
        const type = this.claimForm.controls['types'].value;

        let claim: Claim = {
            status: status,
            reporter: userName,
            description: description,
            title: title,
            claimNumber: claimNumber,
            contactSource: type,
            location: location
        };

        this.claimService.addNewClaim(claim).then(() => {
            this.matSnackBar.open("Hlásenie úspešne uložené", null, {
                duration: 3000,
            });
            this.router.navigate(['claim-list']);
        }, () => {
            this.matSnackBar.open("Vyskytol sa problém", null, {
                duration: 3000,
            });
        })
    }

    private getDataForSelect() {
        this.claimService.getStatuses().subscribe((statusList) => {
            this.statuses = statusList as Status[];
        });

        this.claimService.getLocations().subscribe((locationList) => {
            this.locations = locationList as Location[];
        });

        this.claimService.getNoticeTypes().subscribe((types) => {
            this.noticeTypes = types as NoticeType[];
        });
    }
}
