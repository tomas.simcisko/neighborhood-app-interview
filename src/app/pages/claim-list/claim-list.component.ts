import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../common/services/auth/auth.service';
import { ClaimsService } from '../../common/services/claims/claims.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-claim-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.scss']
})
export class ClaimListComponent implements OnInit {
  public  claims: any[];

  constructor(
      public authService: AuthService,
      private claimService: ClaimsService,
      private router: Router
  ) {}

  ngOnInit(): void {
    this.claimService.getAllClaims().subscribe((value) => {
      this.claims = value;
    });
  }
}
