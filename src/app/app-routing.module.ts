import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ClaimListComponent } from './pages/claim-list/claim-list.component';
import { AuthGuardGuard } from './common/guards/auth-guard.guard';
import { IsSignedInGuard } from './common/guards/is-signed-in.guard';
import { NewClaimComponent } from './pages/new-claim/new-claim.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: "/claim-list",
        pathMatch: 'full'
    },
    {path: 'login', component: LoginComponent, canActivate: [IsSignedInGuard]},
    {path: 'claim-list', component: ClaimListComponent, canActivate: [AuthGuardGuard]},
    {path: 'new-claim', component: NewClaimComponent, canActivate: [AuthGuardGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
