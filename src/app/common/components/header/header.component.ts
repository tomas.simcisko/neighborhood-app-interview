import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { ClaimsService } from '../../services/claims/claims.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input()
  public title: string;

  public activeRoute: string;

  constructor(
    public authService: AuthService,
    private claimService: ClaimsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activeRoute = this.router.url;
  }

  public addNewClaim() {
    this.router.navigate(['new-claim']);
  }

  public backToList() {
    this.router.navigate(['claim-list']);
  }
}
