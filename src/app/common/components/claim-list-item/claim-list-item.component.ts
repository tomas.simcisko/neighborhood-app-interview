import { Component, Input, OnInit } from '@angular/core';
import { Claim } from '../../models/claim/claim';

@Component({
  selector: 'app-claim-list-item',
  templateUrl: './claim-list-item.component.html',
  styleUrls: ['./claim-list-item.component.scss']
})
export class ClaimListItemComponent implements OnInit {

  @Input()
  claim: Claim;

  panelOpenState = false;

  constructor() { }

  ngOnInit(): void {
  }

}
