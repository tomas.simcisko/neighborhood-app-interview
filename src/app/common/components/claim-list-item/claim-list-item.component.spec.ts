import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimListItemComponent } from './claim-list-item.component';

describe('ClaimListItemComponent', () => {
  let component: ClaimListItemComponent;
  let fixture: ComponentFixture<ClaimListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
