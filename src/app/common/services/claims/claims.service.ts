import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from '../../models/auth/user';
import { Claim } from '../../models/claim/claim';

@Injectable({
  providedIn: 'root'
})
export class ClaimsService {

  constructor(
      private afs: AngularFirestore,
      private afAuth: AngularFireAuth,
      private router: Router,
      private ngZone: NgZone) {

  }

  addNewClaim(claim: Claim) {
    const id: string = this.afs.createId();
    const claimRef: AngularFirestoreDocument<any> = this.afs.collection(`claims`).doc(id);
    const data: Claim = {
      ...claim
    };

    return claimRef.set(data, { merge: true });
  }

  getClaimById(id: string) {
    return this.afs.collection('claims').doc(id);
  }

  public getAllClaims() {
    return this.afs.collection('claims').valueChanges({ idField: 'id' });
  }

  public getLocations() {
    return this.afs.collection('locations').valueChanges({ idField: 'id' });
  }

  public getStatuses() {
    return this.afs.collection('status').valueChanges({ idField: 'id' });
  }

  public getNoticeTypes() {
    return this.afs.collection('notice_type').valueChanges({ idField: 'id' });
  }
}
