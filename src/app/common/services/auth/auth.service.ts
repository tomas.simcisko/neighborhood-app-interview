import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from '../../models/auth/user';
import { auth } from 'firebase/app';
import UserCredential = firebase.auth.UserCredential;
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private userData: firebase.User;
  private user: User;

  constructor(
      private afs: AngularFirestore,
      private afAuth: AngularFireAuth,
      private router: Router,
      private ngZone: NgZone
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.getUserById(this.userData.uid).valueChanges().subscribe((user) => {
          this.user = user as User;
        });

        // localStorage.setItem('user', JSON.stringify(this.userData));
        // JSON.parse(localStorage.getItem('user'));


      } else {
        localStorage.setItem('user', null);
      }
    });
  }

  // Login in with email/password
  signIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  // Register user with email/password
  registerUser(email, password) {
    return this.afAuth.createUserWithEmailAndPassword(email, password);
  }

  // Email verification when new user registered
  sendVerificationMail() {
    let user: User = null;

    return this.afAuth.currentUser.then(u => {
      u.sendEmailVerification();
      user = {
        uid: u.uid,
        email: u.email
      };
    })
        .then(() => {

          // this.router.navigate(['verify-email']);
        });
  }

  // Recover password
  recoverPassword(passwordResetEmail) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
        .then(() => {
          window.alert('Password reset email has been sent, please check your inbox.');
        }).catch((error) => {
          window.alert(error);
        });
  }

  resetPasswordInit(email: string) {
    return this.afAuth.sendPasswordResetEmail(
        email,
        { url: 'http://localhost:4200/auth' });
  }

  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // Returns true when user's email is verified
  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user.emailVerified !== false) ? true : false;
  }

  get userFullName(): string {
    const name = this.userData.displayName;
    return name ? name : "";
  }

  // Sign in with Gmail
  async loginWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    const credential: UserCredential = await this.afAuth.signInWithPopup(provider);

    this.setUserData(credential.user);
    localStorage.setItem('user', JSON.stringify(credential.user));

    return this.updateUserData(credential.user);
  }

  // Auth providers
  authLogin(provider) {
    return this.afAuth.signInWithPopup(provider)
        .then((result) => {
          this.ngZone.run(() => {
            this.router.navigate(['role']);
          });
          this.setUserData(result.user);
        }).catch((error) => {
          window.alert(error);
        });
  }

  // Store user in localStorage
  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    };

    return userRef.set(userData, {
      merge: true
    });
  }

  // Sign-out
  signOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }

  // create or update user in db
  updateUserData(user: User) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email
    };

    return userRef.set(data, { merge: true });
  }

  getUserById(uid: string) {
    return this.afs.collection('users').doc(uid);
  }
}
