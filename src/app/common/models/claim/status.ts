export interface Status {
    id?: string;
    color?: string;
    icon?: string;
    text: string;
    value: string;
}
