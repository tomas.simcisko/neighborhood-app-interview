export interface NoticeType {
    id?: string;
    name: string;
    value: string;
    icon: string;
}
