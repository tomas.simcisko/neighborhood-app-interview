import { Status } from './status';
import { Location } from './location';

export interface Claim {
    id?: string;
    title: string;
    claimNumber: string;
    location: Location;
    description?: string;
    status: Status;
    contactSource: string;
    reporter: string;
}
